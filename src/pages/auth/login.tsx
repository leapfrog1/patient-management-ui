import authorize from '../../core/oauth2/authenticator';

const Login = (): JSX.Element => {
  return (
    <button onClick={() => {
      authorize();
    }} className='primary-button'>Login</button>
  );
};

export default Login;
