import React, { FC, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from '../../../core/root-redux/store';
import ToggleButton from '../../../shared/components/form/button/toggle-button';
import Contact from '../../../shared/components/form/contact/contact';
import DateInput from '../../../shared/components/form/date/date';
import Email from '../../../shared/components/form/email/email';
import { Form, FormDef } from '../../../shared/components/form/form';
import Name from '../../../shared/components/form/name/name';
import { ImgUploader } from '../../../shared/components/image-uploader/img-uploader';
import { controls, FormItems } from '../../../shared/constants/form-item.constant';
import { isValidImage } from '../../../shared/utils/image.util';
import { defaultPatientData, Patient } from '../model/patient';
import { changePage, search, toggleDrawer } from '../redux/patient-slice';
import { useSavePatientMutation } from '../service/patient-service';

const setFormProps = (formData: Patient, formKey: string, setFormData: (value: React.SetStateAction<Patient>) => void): React.InputHTMLAttributes<HTMLInputElement> => {
  return {
    value: (formData?.[formKey as keyof Patient]?.toString() ?? ''), onInput: (event) => setFormData({ ...formData, [formKey]: event.currentTarget.value })
  };
};
const getPatientData = (formData: Patient, photo?: File): FormData => {
  const patientData = new FormData();
  photo && isValidImage(photo) && patientData.set('photo', photo);
  patientData.set('patient', new Blob([JSON.stringify(formData)], {
    type: 'application/json'
  }));
  return patientData;
};

const PatientForm: FC = () => {
  const dispatch = useAppDispatch();
  const { paginator, formData } = useSelector((state: RootState) => state.patient);
  const [savePatient] = useSavePatientMutation(defaultPatientData as Object);
  const isUpdate = (formData?.id ?? 0) > 0;
  const [newFormData, setFormData] = useState(formData);

  const onSubmit = ({ form, isInvalid, data }: FormDef): void => {
    if (isInvalid) return;

    const patientData = getPatientData(newFormData, data.get('photo') as File);

    if (isUpdate) {
      savePatient(patientData);
      return;
    }

    savePatient(patientData).then(() => {
      form.reset();
      setFormData(defaultPatientData);
      dispatch(changePage({ pageable: { ...paginator.pageable, activePage: 1 } }));
      dispatch(search(''));
      dispatch(toggleDrawer(null));
    });
  };

  const { firstName, middleName, lastName, dob, emailAddress, mobileNumber, specialAttention, photo } = controls;
  return (
    <Form baseProps={{ className: 'pt-2r pb-2r' }} submit={(event: FormDef) => onSubmit(event)}>
      <ImgUploader className='py-2r' value={newFormData.tempImagePath} name={photo} />
      <ToggleButton baseProps={{ name: specialAttention }} value={newFormData.specialAttention} toggle={(value) => setFormData({ ...newFormData, [specialAttention]: value })} className='p-2r dark-bg my-2r' id='specialAttention'>Special Attention</ToggleButton>
      <Name inputProps={setFormProps(newFormData, firstName, setFormData)} formItem={FormItems[firstName]}></Name>
      <Name inputProps={setFormProps(newFormData, middleName, setFormData)} formItem={FormItems[middleName]}></Name>
      <Name inputProps={setFormProps(newFormData, lastName, setFormData)} formItem={FormItems[lastName]}></Name>
      <Email inputProps={setFormProps(newFormData, emailAddress, setFormData)} formItem={FormItems[emailAddress]}></Email>
      <Contact inputProps={setFormProps(newFormData, mobileNumber, setFormData)} formItem={FormItems[mobileNumber]}></Contact>
      <DateInput inputProps={setFormProps(newFormData, dob, setFormData)} formItem={FormItems[dob]}></DateInput>
      <div className="flex flex--jsc w-100 mt-2r">
        <button type='submit' className="primary-button pl-3r pr-3r p-1r mr-1r">{isUpdate ? 'Update Patient' : 'Add Patient'}</button>
      </div>
    </Form >
  );
};
export default PatientForm;
