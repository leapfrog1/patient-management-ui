import { createApi } from '@reduxjs/toolkit/query/react';
import fetchQuery from '../../../core/http-client/rtk-query';
import { PageableData } from '../../../shared/components/table/model/pageable';
import { Pageable } from '../../../shared/components/table/paginator/page-numbers';
import { apiURLs } from '../../../shared/constants/api-url.constant';
import { Patient } from '../model/patient';

export const patientService = createApi({
  reducerPath: 'patientService',
  baseQuery: fetchQuery(),
  refetchOnMountOrArgChange: 0.1,
  tagTypes: ['PATIENT'],
  endpoints: (builder) => ({
    getPageableData: builder.query<PageableData<Patient>, Pageable & { query?: string }>({
      query: ({ activePage, pageCapacity, query }) =>
        `${apiURLs.patients}?page=${activePage - 1}&size=${pageCapacity}&query=${
          query ?? ''
        }&sort=id,desc`,
      providesTags: (result) =>
        result
          ? [
              ...result.data.map(({ id }) => ({ type: 'PATIENT' as const, id })),
              { type: 'PATIENT', id: 'LIST' }
            ]
          : [{ type: 'PATIENT', id: 'LIST' }]
    }),
    savePatient: builder.mutation<Patient, FormData>({
      query: (body) => ({
        url: `${apiURLs.patients}`,
        method: 'POST',
        body
      }),
      invalidatesTags: [{ type: 'PATIENT', id: 'LIST' }]
    }),
    updateSpecialAttention: builder.mutation<Patient, { id: string; specialAttention: boolean }>({
      query: ({ id, specialAttention }) => ({
        url: `${apiURLs.patients}/${id}`,
        method: 'PATCH',
        body: { specialAttention }
      }),
      invalidatesTags: (result) =>
        result ? [{ type: 'PATIENT', id: result.id }] : [{ type: 'PATIENT', id: 'LIST' }]
    }),
    deletePatient: builder.mutation<Patient, { id: string }>({
      query: ({ id }) => ({
        url: `${apiURLs.patients}/${id}`,
        method: 'DELETE'
      }),
      invalidatesTags: [{ type: 'PATIENT', id: 'LIST' }]
    })
  })
});

export const {
  useGetPageableDataQuery,
  useSavePatientMutation,
  useUpdateSpecialAttentionMutation,
  useDeletePatientMutation
} = patientService;
