import { Identity } from '../../../shared/models/identity';
import { Name } from '../../../shared/models/name';

export interface Patient extends Identity<number>, Name {
  mobileNumber: string;
  emailAddress: string;
  dob: Date | string;
  specialAttention: boolean;
  photo?: FormData;
  imagePath?: string;
  tempImagePath?: string;
}
export const defaultPatientData: Patient = Object.freeze({
  id: 0,
  dob: '',
  emailAddress: '',
  firstName: '',
  lastName: '',
  mobileNumber: '',
  middleName: '',
  specialAttention: false,
  tempImagePath: '',
  imagePath: ''
});
