import { createSlice, PayloadAction, SliceCaseReducers } from '@reduxjs/toolkit';
import {
  emptyData,
  initialPage,
  PageableData
} from '../../../shared/components/table/model/pageable';
import { PageNumberProps } from '../../../shared/components/table/paginator/page-numbers';
import { defaultPatientData, Patient } from '../model/patient';
import { patientService } from '../service/patient-service';

const paginator: PageNumberProps = {
  pageable: initialPage()
};

interface PatientState {
  paginator: PageNumberProps;
  query: string;
  drawerState: boolean;
  data: PageableData<Patient>;
  formData: Patient;
}

export const patientSlice = createSlice<PatientState, SliceCaseReducers<PatientState>, string>({
  name: 'patient',
  initialState: {
    paginator,
    query: '',
    drawerState: false,
    data: emptyData<Patient>(),
    formData: defaultPatientData
  },
  reducers: {
    toggleDrawer: (state) => {
      state.drawerState = !state.drawerState;
      state.formData = defaultPatientData;
    },
    search: (state, action: PayloadAction<string>) => {
      state.query = action.payload;
    },
    changePage: (state, action: PayloadAction<PageNumberProps>) => {
      state.paginator = action.payload;
    },
    openDrawerWith: (state, action: PayloadAction<Patient>) => {
      state.formData = action.payload;
      state.drawerState = true;
    }
  },
  extraReducers: (builder) => {
    builder
      .addMatcher(patientService.endpoints.getPageableData.matchFulfilled, (state, { payload }) => {
        state.paginator.pageable.totalPages = payload.totalPages ?? 1;
        state.data = payload;
      });
  }
});

export const { toggleDrawer, search, changePage, openDrawerWith } = patientSlice.actions;

export default patientSlice.reducer;
