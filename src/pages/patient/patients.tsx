import React from 'react';
import PatientWidget from './action-widget/patient-action-widget';

import PatientList from './table/patient-list';
const Patient: React.FC = () => {
  return (
    <div>
      <PatientList></PatientList>
    </div>
  );
};

export default Object.assign(Patient, {
  ActionBar: PatientWidget
});
