import { BaseColumn } from '../../../shared/components/table/table-header/table-header';

const patientColumns: BaseColumn[] = [
  {
    name: 'name',
    title: 'Name'
  },
  {
    name: 'emailAddress',
    title: 'Email'
  },
  {
    name: 'mobileNumber',
    title: 'Mobile Number'
  },
  {
    name: 'dob',
    title: 'Date of Birth'
  },
  {
    name: 'specialAttention',
    title: 'Special Attention'
  }
];

export default structuredClone(patientColumns);
