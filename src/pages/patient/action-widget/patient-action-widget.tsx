import React, { HTMLAttributes } from 'react';
import { useSelector } from 'react-redux';
import { RootState, useAppDispatch } from '../../../core/root-redux/store';
import Drawer from '../../../shared/components/drawer/drawer';
import SearchBar from '../../../shared/components/filter/search/search-bar';
import PatientForm from '../form/patient-form';
import { changePage, search, toggleDrawer } from '../redux/patient-slice';

export interface PatientWidgetState {
  drawerOpened: boolean
  searchQuery?: string
}
const PatientWidget: React.FC<HTMLAttributes<{}>> = (props) => {
  const { drawerState, paginator } = useSelector((state: RootState) => state.patient);
  const dispatch = useAppDispatch();
  const onSearch = (newQuery: string): void => {
    dispatch(search(newQuery));
    dispatch(changePage({ pageable: { ...paginator.pageable, activePage: 1 } }));
  };
  return (
    <div className={`flex ${props.className ?? ''}`}>
      <Drawer closeDrawer={() => dispatch(toggleDrawer(null))} open={drawerState}>
        <PatientForm></PatientForm>
      </Drawer>
      <SearchBar
        search={onSearch}
        inputProps={{
          placeholder: 'Search by Email, name or mobile number'
        }} className="w-100"></SearchBar>
      <button onClick={() => dispatch(toggleDrawer(null))} className="primary-button primary-button--outline">Add Patient</button>
    </div>
  );
};

export default PatientWidget;
