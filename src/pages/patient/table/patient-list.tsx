import { MutationTrigger } from '@reduxjs/toolkit/dist/query/react/buildHooks';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../core/root-redux/store';
import { initialPage, PageableData } from '../../../shared/components/table/model/pageable';
import { PageNumberProps } from '../../../shared/components/table/paginator/page-numbers';
import Paginator from '../../../shared/components/table/paginator/paginator';
import { BasicTable } from '../../../shared/components/table/table';
import { Patient } from '../model/patient';
import { changePage, openDrawerWith } from '../redux/patient-slice';
import { useDeletePatientMutation, useGetPageableDataQuery, useUpdateSpecialAttentionMutation } from '../service/patient-service';
import { toBasicTableProps } from './patient-mapper';

const renderTable = (page: PageNumberProps, data: PageableData<Patient>, dispatch: (action: any) => void, updateSpecialAttention: MutationTrigger<any>, deletePatient: MutationTrigger<any>): JSX.Element => {
  return (
    <div>
      <BasicTable {...toBasicTableProps(data, {
        onRowClick: (data) => dispatch(openDrawerWith(data)),
        changeSpecialAttention: (id, specialAttention) => { updateSpecialAttention({ id, specialAttention }); },
        deletePatient: (id) => { deletePatient({ id }); }
      })}></BasicTable>
      <div className="mt-2r">
        <Paginator {...page} pageChange={activePage => dispatch(changePage({ pageable: { ...page.pageable, activePage } }))} ></Paginator>
      </div>
    </div>
  );
};

const renderLoader = (): JSX.Element => {
  return (
    <div>Loading....</div>
  );
};

const renderDataError = (): JSX.Element => {
  return (
    <div>Error while fetching data</div>
  );
};

const renderBlankData = (): JSX.Element => {
  return (
    <div>
      Data not found
    </div>
  );
};

const PatientList = (): JSX.Element => {
  const { query, paginator } = useSelector((state: RootState) => state.patient);
  const { data, error, isLoading } = useGetPageableDataQuery({ ...initialPage(), activePage: paginator.pageable.activePage, query });
  const [updateSpecialAttention] = useUpdateSpecialAttentionMutation();
  const [deletePatient] = useDeletePatientMutation();
  const dispatch = useDispatch();
  if (error) {
    return renderDataError();
  }
  if (isLoading) {
    return renderLoader();
  }
  if (!data || (data && data.data?.length === 0)) {
    return renderBlankData();
  }
  return renderTable(paginator, data, dispatch, updateSpecialAttention, deletePatient);
};

export default PatientList;
