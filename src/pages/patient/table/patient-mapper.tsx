import ToggleButton from '../../../shared/components/form/button/toggle-button';
import { PageableData } from '../../../shared/components/table/model/pageable';
import { BasicTableProps } from '../../../shared/components/table/table';
import TD from '../../../shared/components/table/table-body/table-data';
import ATR from '../../../shared/components/table/table-row/action-table-row';
import TR from '../../../shared/components/table/table-row/table-row';
import { toMediumDate } from '../../../shared/utils/date.util';
import { toFullName } from '../../../shared/utils/name.util';
import patientColumns from '../constants/patient.constant';
import { Patient } from '../model/patient';

export interface PatientTableAction {
  onRowClick?: (patient: Patient) => void;
  deletePatient?: (id: string) => void;
  changeSpecialAttention?: (id: string, specialAttention: boolean) => void;
}

export const renderNameColumn = (fullName: string, imagePath?: string): JSX.Element | string => {
  if (!imagePath) {
    return fullName;
  }
  return (
    <div className='flex flex--ac flex--jsc'>
      <img className='round-image round-image--small' src={imagePath} alt={fullName} />
      <span className='ml-2r'>{fullName}</span>
    </div>
  );
};

export const toBasicTableProps = (patients: PageableData<Patient>, { onRowClick, changeSpecialAttention, deletePatient }: PatientTableAction): BasicTableProps => {
  return {
    columns: patientColumns,
    data: patients.data
      .map((patient) => {
        return <TR trBaseProps={{ onClick: () => onRowClick?.(patient) }} key={patient.id}>
          <TD datum={renderNameColumn(toFullName(patient), patient.tempImagePath)}></TD>
          <TD datum={patient.emailAddress}></TD>
          <TD datum={patient.mobileNumber}></TD>
          <TD datum={toMediumDate(patient.dob)}></TD>
          <TD datum={patient.specialAttention ? <span className='red-flag'>YES</span> : <span className='green-flag'>NO</span>}></TD>
          <ATR>
            <ToggleButton value={patient?.specialAttention} toggle={(value, event) => {
              event?.stopPropagation();
              changeSpecialAttention?.(patient.id.toString(), value);
            }} className='p-1r dark-bg' id={`specialAttention_${patient.id}`}>Special Attention</ToggleButton>
            <button onClick={($event) => {
              $event.stopPropagation();
              deletePatient?.(patient.id.toString());
            }} className='basic-button primary-border border-rad-0'>Delete</button>
          </ATR>
        </TR>;
      })
  };
};
