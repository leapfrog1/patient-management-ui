import { Component, ReactNode } from 'react';
import authorize, { isLoggedIn } from './core/oauth2/authenticator';
import Router from './core/router/router';

interface AppState {
  isLoggedIn: boolean;
}
class App extends Component<{}, AppState> {
  state: Readonly<AppState> = {
    isLoggedIn: isLoggedIn()
  };

  componentDidMount(): void {
    authorize();
    window.addEventListener('storage', () => {
      if (this.state.isLoggedIn && !isLoggedIn()) {
        authorize();
      }
      this.setState({
        isLoggedIn: isLoggedIn()
      });
    });
  }

  render(): ReactNode {
    if (!this.state.isLoggedIn) {
      return;
    }
    return (
      <div>
        <Router />
      </div>
    );
  }
}

export default App;
