import { isEmpty } from './string.util';

export const isValidImage = (imageFile: File): boolean => {
  const { name, size, type } = imageFile;
  return type.includes('image/jpeg') && !isEmpty(name) && size > 0;
};

export const encode = (data: Uint8Array): string => {
  return (
    'data:image/jpeg;base64,' +
    window
      .btoa(data.reduce((a, b) => a + String.fromCharCode(b), ''))
      .replace(/.{76}(?=.)/g, '$&\n')
  );
};

export const dataURLToBlob = (dataURL: string): Blob => {
  const BASE64_MARKER = ';base64,';
  if (!dataURL.includes(BASE64_MARKER)) {
    const parts = dataURL.split(',');
    const contentType = parts[0].split(':')[1];
    const raw = parts[1];
    return new Blob([raw], { type: contentType });
  }

  const parts = dataURL.split(BASE64_MARKER);
  const contentType = parts[0].split(':')[1];
  const raw = window.atob(parts[1]);
  const rawLength = raw.length;

  const uInt8Array = new Uint8Array(rawLength);

  for (let i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }

  return new Blob([uInt8Array], { type: contentType });
};

const imageSize = {
  large: {
    width: 200,
    height: 200
  },
  medium: {
    width: 100,
    height: 100
  },
  small: {
    width: 50,
    height: 50
  }
};

const resizeImage = (
  image: HTMLImageElement,
  size: 'large' | 'medium' | 'small'
): string | null => {
  document.getElementById('root')?.append?.(document.createElement('canvas'));
  const canvas: HTMLCanvasElement = document.getElementsByTagName('canvas')?.[0];
  if (!canvas) {
    return null;
  }
  canvas.width = imageSize[size].width * 2;
  canvas.height = imageSize[size].height * 2;
  const ctx = canvas.getContext('2d');
  ctx?.drawImage(image, 0, 0, image.width, image.height, 0, 0, canvas.width, canvas.height);
  document.getElementsByTagName('canvas')?.[0].remove();
  return canvas.toDataURL('image/jpeg');
};

export default resizeImage;
