export const toMediumDate = (value: Date | string): string => {
  return new Date(value).toLocaleDateString('en-us', {
    year: 'numeric',
    month: 'short',
    day: 'numeric'
  });
};
