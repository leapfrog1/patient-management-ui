import { Name } from '../models/name';

export const toFullName = ({ firstName, middleName, lastName }: Name): string => {
  return firstName + ' ' + (middleName ?? '') + ' ' + lastName;
};
