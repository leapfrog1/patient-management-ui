import { HTMLAttributes } from 'react';
import DropdownItem from './drop-item';

export interface DropdownItemsProps {
  className?: string;
  baseProps?: HTMLAttributes<HTMLUListElement>;
  children?: JSX.Element[] | JSX.Element;
  items?: string[];
}

const renderItems = (items?: string[]): JSX.Element[] | null => {
  if ((items?.length ?? 0) === 0) {
    return null;
  }
  return (
    items!.map((value, index) =>
      <DropdownItem key={String(index) + value}>{value}</DropdownItem>
    )
  );
};

const DropdownItems: React.FC<DropdownItemsProps> = ({ children, baseProps, items, className = '' }: DropdownItemsProps) => {
  return (
    <ul {...baseProps} className={`dropdown__items ${className}`}>
      {children ?? renderItems(items)}
    </ul>
  );
};

export default DropdownItems;
