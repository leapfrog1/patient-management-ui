import { LiHTMLAttributes } from 'react';

export interface DropdownItemProps {
  className?: string;
  baseProps?: LiHTMLAttributes<HTMLLIElement>;
  children?: JSX.Element | JSX.Element[] | string;
}

const DropdownItem: React.FC<DropdownItemProps> = ({ baseProps, children, className = '' }: DropdownItemProps) => {
  return (
    <li {...baseProps} className={`dropdown__item ${className}`}>
      {children}
    </li>
  );
};

export default DropdownItem;
