import React, { HTMLAttributes, useState } from 'react';
import DropdownItem from './drop-item';
import DropdownItems from './dropdown-items';

export interface DropdownProps {
  baseProps?: HTMLAttributes<HTMLDivElement>;
  className?: string;
  label: string | JSX.Element;
  children?: JSX.Element;
}

const Dropdown: React.FC<DropdownProps> = ({ baseProps, label, children, className = '' }: DropdownProps): JSX.Element => {
  const [state, toggle] = useState(false);
  return (
    <div onClick={() => toggle(!state)} {...baseProps} className={`dropdown ${className} ${state ? 'dropdown--active' : ''}`}>
      <span>{label}</span>
      {state ? children : ''}
    </div>
  );
};

export default Object.assign(Dropdown, {
  Items: DropdownItems,
  Item: DropdownItem
});
