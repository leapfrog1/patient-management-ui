import React from 'react';
import { TRProps } from './table-row';

export interface AtrProps extends TRProps {
}

const ATR: React.FC<TRProps> = (props: AtrProps): JSX.Element => {
  return (
    <td className='action-row'>
      {props.children}
    </td>
  );
};

export default ATR;
