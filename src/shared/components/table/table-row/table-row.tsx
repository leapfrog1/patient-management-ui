import React, { HTMLAttributes, ReactNode } from 'react';

export interface TRProps {
  children?: ReactNode
  trBaseProps?: HTMLAttributes<HTMLTableRowElement>
}

const TR: React.FC<TRProps> = ({ children, trBaseProps }: TRProps): JSX.Element => {
  return (
    <tr {...trBaseProps} className='interactive-row'>
      {children}
    </tr>
  );
};

export default TR;
