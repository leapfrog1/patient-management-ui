import React, { TdHTMLAttributes } from 'react';

export interface BaseTableData {
  datum: JSX.Element | string | number | JSX.Element[]
  baseProps?: TdHTMLAttributes<HTMLTableCellElement>
}

const TD: React.FunctionComponent<BaseTableData> = ({ datum, baseProps }: BaseTableData) => {
  return (
    <td {...baseProps} className="base-td">{datum}</td>
  );
};

export default TD;
