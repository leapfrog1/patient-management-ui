import React, { HTMLAttributes } from 'react';
import Icon from '../../icons/svg-icon';
import Icons from '../../icons/svg-icon.constant';

export interface FirstPageProps {
  goToFirstPage: () => void
}
const GoToFirstPage: React.FC<FirstPageProps & HTMLAttributes<HTMLButtonElement>> = ({ goToFirstPage, ...props }: FirstPageProps & HTMLAttributes<HTMLButtonElement>) => {
  return (
        <button onClick={() => goToFirstPage()} {...props} className={`basic-round-button ${props?.className ?? ''}`}>
            <Icon {...Icons.firstIndicator}></Icon>
        </button>
  );
};

export default GoToFirstPage;
