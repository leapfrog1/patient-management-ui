import { logout, navigateToUserAccount } from '../../../core/oauth2/authenticator';
import Dropdown from '../dropdown/dropdown';

export const Avatar = (): JSX.Element => {
  return (
    <Dropdown label='Account'>
      <Dropdown.Items>
        <Dropdown.Item baseProps={{ onClick: () => navigateToUserAccount() }}>Manage your account</Dropdown.Item>
        <Dropdown.Item baseProps={{ onClick: () => logout() }}>Logout</Dropdown.Item>
      </Dropdown.Items>
    </Dropdown>
  );
};
