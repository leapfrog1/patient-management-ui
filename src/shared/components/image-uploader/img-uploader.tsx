import { ChangeEvent, FC, useState } from 'react';
import dummyImage from '../../../assets/images/dummy-person.png';
import resizeImage, { dataURLToBlob } from '../../utils/image.util';

export interface ImageUploaderProps {
  name?: string;
  id?: string;
  value?: string;
  className?: string;

}
const renderImage = (id: string, image: File, imageSrc: any, setImageSrc: any): JSX.Element => {
  const fileReader = new FileReader();
  fileReader.readAsDataURL(image);
  fileReader.onload = (): void => {
    const img = new Image();
    img.onload = (): void => {
      const resizedImage = resizeImage(img, 'medium');
      resizedImage && updateFileSelector(id, image.name, resizedImage);
      setImageSrc(resizedImage);
    };
    img.src = fileReader.result?.toString() ?? imageSrc;
  };
  return (
    <img className='round-image round-image--medium' src={imageSrc ?? dummyImage} onError={(event) => { event.currentTarget.src = dummyImage; }} ></img >
  );
};

const updateFileSelector = (id: string, fileName: string, resizedImage: string): void => {
  const fileInput = document.getElementById(id) as HTMLInputElement;
  const resizedFile = new File([dataURLToBlob(resizedImage)], fileName, { lastModified: Date.now(), type: 'image/jpeg' });
  const dataTransfer = new DataTransfer();
  dataTransfer.items.add(resizedFile);
  fileInput.files = dataTransfer.files;
};

export const ImgUploader: FC<ImageUploaderProps> = ({ className = '', id = 'file-uploader', name, value }): JSX.Element => {
  const [imageSrc, setImageSrc] = useState(value);
  const [image, setImage] = useState(new File([new Blob([])], ''));
  return (
    <div className={`flex flex--ac flex--jsc ${className}`}>
      <input name={name} accept="image/png, image/jpeg" style={{ visibility: 'hidden' }} id={id} className='w-0 h-0' type="file" onChange={(event: ChangeEvent<HTMLInputElement>) => setImage(event.currentTarget.files?.[0] ?? image)} />
      <label htmlFor={id}>
        {renderImage(id, image, imageSrc, setImageSrc)}
      </label>
    </div>
  );
};

export default ImgUploader;
