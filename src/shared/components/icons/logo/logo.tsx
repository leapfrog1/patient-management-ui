import Icon from '../svg-icon';
import Icons from '../svg-icon.constant';

export const Logo = (): JSX.Element => {
    return (
        <Icon {...Icons.logo}></Icon>
    );
};
