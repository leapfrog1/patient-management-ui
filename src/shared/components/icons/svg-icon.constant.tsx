import { Color } from '../../constants/color.constant';
import { IconProps } from './svg-icon';

const Icons: { [key: string]: IconProps } = Object.freeze({
    logo: Object.freeze({
        width: '3.5rem',
        height: '3.5rem',
        fill: Color.primary,
        stroke: 'none',
        name: 'logo'
    }),
    patients: Object.freeze({
        width: '2rem',
        height: '1.6rem',
        fill: 'none',
        stroke: Color.inactiveItem,
        name: 'patients'
    }),
    users: Object.freeze({
        width: '2rem',
        height: '1.6rem',
        fill: 'none',
        stroke: Color.inactiveItem,
        name: 'users'
    }),
    firstIndicator: Object.freeze({
        width: '2rem',
        height: '1.4rem',
        fill: 'none',
        stroke: Color.inactiveItem,
        name: 'first-indicator'
    }),
    previousIndicator: Object.freeze({
        width: '2rem',
        height: '1.4rem',
        fill: 'none',
        stroke: Color.inactiveItem,
        name: 'previous-indicator'
    }),
    nextIndicator: Object.freeze({
        width: '2rem',
        height: '1.4rem',
        fill: 'none',
        stroke: Color.inactiveItem,
        name: 'next-indicator'
    }),
    lastIndicator: Object.freeze({
        width: '2rem',
        height: '1.4rem',
        fill: 'none',
        stroke: Color.inactiveItem,
        name: 'last-indicator'
    })
});

export default Icons;
