import React from 'react';
import sprite from '../../../assets/sprites.svg';
export interface IconProps {
  width: string
  height: string
  fill: string
  stroke: string
  name: string
}

const Icon: React.FunctionComponent<IconProps> = ({ width, height, fill, stroke, name }: IconProps) => {
  return (
        <svg width={width} height={height} fill={fill} stroke={stroke} version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
            <use href={`${sprite}#${name}`}></use>
        </svg>
  );
};

export default Icon;
