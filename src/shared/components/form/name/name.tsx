import { FormEvent, PureComponent } from 'react';
import VBaseInput, { BaseInputProps, BaseInputState } from '../base-form-item/base-input';
import { validator } from '../validation-rules/rules';

export interface NameState extends BaseInputState {
}

class Name extends PureComponent<BaseInputProps, NameState> {
  state: Readonly<NameState> = {
    controlValidator: validator(this.props.formItem)
  };

  validateControl(event: FormEvent<HTMLInputElement>): void {
    const errorMessage = this.state.controlValidator?.(event.currentTarget.value) ?? '';
      this.setState({
        errorMessage
      });
      event.currentTarget.setCustomValidity(errorMessage);
  }

  render(): JSX.Element {
    return (
      <div className='form-item' >
        <VBaseInput errorMessage={this.state.errorMessage} {...this.props} inputProps={{
          type: 'text',
          onBlur: (this.validateControl.bind(this)),
          onChange: (this.validateControl.bind(this)),
          ...this.props.inputProps
        }}></VBaseInput>
      </div>
    );
  }
};

export default Name;
