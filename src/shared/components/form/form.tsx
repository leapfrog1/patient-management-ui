import React, { FormEvent, FormHTMLAttributes } from 'react';

export interface FormProps {
  className?: string;
  children?: React.ReactNode;
  baseProps?: FormHTMLAttributes<HTMLFormElement>;
  submit?: (value: FormDef) => void;
}

export interface FormDef {
  form: HTMLFormElement,
  data: FormData;
  isInvalid?: boolean;
}

const validateForm = (formEvent: FormEvent<HTMLFormElement>): boolean => {
  let isInvalid = false;
  Array.from(formEvent.currentTarget.children).forEach(control => {
    const inputElement = control.querySelector('input');
    const dispatchEvent = inputElement?.dispatchEvent.bind(inputElement);
    dispatchEvent?.(new Event('focus', {
      bubbles: true,
      cancelable: true
    }));
    dispatchEvent?.(new Event('focusout', {
      bubbles: true,
      cancelable: true
    }));
    if (!isInvalid) {
      isInvalid = inputElement?.validity?.customError ?? false;
    }
  });
  return isInvalid;
};

const toFormData = (formEvent: FormEvent<HTMLFormElement>): FormDef => {
  formEvent.preventDefault();
  return {
    form: formEvent.currentTarget,
    isInvalid: validateForm(formEvent),
    data: new FormData(formEvent.currentTarget)
  };
};

export const Form: React.FC<FormProps> = ({ submit, children, baseProps, className }: FormProps) => {
  return (
    <form onSubmit={($event) => submit?.(toFormData($event))} className={className} {...baseProps}>
      {children}
    </form>
  );
};
