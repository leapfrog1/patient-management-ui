import { FC, InputHTMLAttributes, LabelHTMLAttributes } from 'react';
import Label from '../label/label';
import { FormItem } from '../validation-rules/rules';

export interface BaseInputProps {
  formItem: FormItem,
  errorMessage?: string;
  inputProps?: InputHTMLAttributes<HTMLInputElement>
  labelProps?: LabelHTMLAttributes<HTMLLabelElement>
}

export interface BaseInputState {
  errorMessage?: string;
  controlValidator?: (value: string) => string | undefined;
}
const renderErrorMessage = (errorMessage?: string): JSX.Element | null => {
  if (errorMessage) {
    return <span className='mt-1r ml-1r error-alert'>{errorMessage}</span>;
  }
  return null;
};

const VBaseInput: FC<BaseInputProps> = ({ errorMessage, formItem, labelProps, inputProps }: BaseInputProps) => {
  const { placeholder, name } = formItem;
  return (
    <div className='v-form-item'>
      <Label {...labelProps} formItem={formItem}></Label>
      <input {...inputProps} placeholder={placeholder} name={name} className='v-form-item__input basic-input' />
      {renderErrorMessage(errorMessage)}
    </div>
  );
};

export default VBaseInput;
