import { FormEvent, PureComponent, ReactNode } from 'react';
import VBaseInput, { BaseInputProps, BaseInputState } from '../base-form-item/base-input';
import { validator } from '../validation-rules/rules';

export interface DateState extends BaseInputState {
}

class DateInput extends PureComponent<BaseInputProps, DateState> {
  state: Readonly<DateState> = {
    controlValidator: validator(this.props.formItem)
  };

  validateControl(event: FormEvent<HTMLInputElement>): void {
    const errorMessage = this.state.controlValidator?.(event.currentTarget.value) ?? '';
    this.setState({
      errorMessage
    });
    event.currentTarget.setCustomValidity(errorMessage);
  }

  render(): ReactNode {
    return (
      <div className='form-item'>
        <VBaseInput {...this.props} errorMessage={this.state.errorMessage} inputProps={{
          onBlur: (this.validateControl.bind(this)),
          onChange: (this.validateControl.bind(this)),
          type: 'date',
          ...this.props.inputProps
        }}></VBaseInput>
      </div>
    );
  }
};

export default DateInput;
