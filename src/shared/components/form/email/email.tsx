import { FormEvent, PureComponent, ReactNode } from 'react';
import VBaseInput, { BaseInputProps, BaseInputState } from '../base-form-item/base-input';
import { validator } from '../validation-rules/rules';

export interface EmailState extends BaseInputState {
}

class Email extends PureComponent<BaseInputProps, EmailState> {
  state: Readonly<EmailState> = {
    controlValidator: validator(this.props.formItem)
  };

  validateControl(event: FormEvent<HTMLInputElement>): void {
    const errorMessage = this.state.controlValidator?.(event.currentTarget.value) ?? '';
    this.setState({
      errorMessage
    });
    event.currentTarget.setCustomValidity(errorMessage);
  }

  render(): ReactNode {
    return (
      <div className='form-item'>
        <VBaseInput errorMessage={this.state.errorMessage} {...this.props} inputProps={{
          onBlur: (this.validateControl.bind(this)),
          onChange: (this.validateControl.bind(this)),
          type: 'email',
          ...this.props.inputProps
        }}></VBaseInput>
      </div>
    );
  }
};

export default Email;
