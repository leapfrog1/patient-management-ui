import React, { InputHTMLAttributes } from 'react';

export interface ToggleButtonProps {
  className?: string;
  id: string;
  children: string;
  baseProps?: InputHTMLAttributes<HTMLInputElement>;
  value?: boolean;
  toggle?: (value: boolean, event?: React.ChangeEvent<HTMLInputElement>) => void;
}

const ToggleButton: React.FC<ToggleButtonProps> = ({ id, baseProps, value, children, toggle, className = '' }) => {
  return (
    <div onClick={($event) => $event.stopPropagation()} className={`secondary-toggle ${className}`}>
      <label className='secondary-toggle__label' htmlFor={id}>
        {children}
      </label>
      <input {...baseProps} checked={value} onChange={($event) => toggle?.($event.currentTarget.checked)} className='secondary-toggle__input' id={id} type='checkbox' />
      <label className='secondary-toggle__container' htmlFor={id}></label>
    </div>
  );
};

export default ToggleButton;
