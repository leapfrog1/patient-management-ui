import {
  FormItem,
  maxLength,
  minLength,
  pattern,
  required
} from '../components/form/validation-rules/rules';
import {
  maxLengthTemplate,
  minLengthTemplate,
  regexTemplate,
  requiredTemplate
} from '../components/form/validation-rules/rules.constant';

export const controls = {
  firstName: 'firstName',
  middleName: 'middleName',
  lastName: 'lastName',
  emailAddress: 'emailAddress',
  dob: 'dob',
  mobileNumber: 'mobileNumber',
  specialAttention: 'specialAttention',
  photo: 'photo'
};

export const FormItems: Readonly<{ [key: string]: FormItem }> = {
  [controls.firstName]: {
    name: controls.firstName,
    placeholder: 'Example: John',
    title: 'First Name',
    rules: [
      {
        type: required,
        value: true,
        message: requiredTemplate('First Name')
      },
      {
        type: minLength,
        value: 2,
        message: minLengthTemplate('First Name', '2')
      },
      {
        type: maxLength,
        value: 50,
        message: maxLengthTemplate('First Name', '50')
      }
    ]
  },
  [controls.middleName]: {
    name: controls.middleName,
    placeholder: 'Example: Michael (Optional)',
    title: 'Middle Name',
    rules: [
      {
        type: minLength,
        value: 2,
        message: minLengthTemplate('Middle Name', '2')
      },
      {
        type: maxLength,
        value: 50,
        message: maxLengthTemplate('Middle Name', '50')
      }
    ]
  },
  [controls.lastName]: {
    name: controls.lastName,
    placeholder: 'Example: Doe (Optional)',
    title: 'Last Name',
    rules: [
      {
        type: required,
        value: true,
        message: requiredTemplate('Last Name')
      },
      {
        type: minLength,
        value: 2,
        message: minLengthTemplate('Last Name', '2')
      },
      {
        type: maxLength,
        value: 50,
        message: maxLengthTemplate('Last Name', '50')
      }
    ]
  },
  [controls.emailAddress]: {
    name: controls.emailAddress,
    placeholder: 'Example: john.doe@gmail.com',
    title: 'Email',
    rules: [
      {
        type: required,
        value: true,
        message: requiredTemplate('Email')
      },
      {
        type: pattern,
        value: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
        message: regexTemplate('Email')
      }
    ]
  },
  [controls.mobileNumber]: {
    name: controls.mobileNumber,
    placeholder: 'Example: 9841234532',
    title: 'Contact',
    rules: [
      {
        type: required,
        value: true,
        message: requiredTemplate('Contact')
      },
      {
        type: pattern,
        value: /[9][6-9]\d{8}/,
        message: regexTemplate('Contact')
      },
      {
        type: maxLength,
        value: 10,
        message: maxLengthTemplate('Contact', '10')
      },
      {
        type: minLength,
        value: 10,
        message: minLengthTemplate('Contact', '10')
      }
    ]
  },
  [controls.dob]: {
    name: controls.dob,
    title: 'Date of Birth',
    rules: [
      {
        type: required,
        value: true,
        message: requiredTemplate('Date of Birth')
      }
    ]
  }
};
