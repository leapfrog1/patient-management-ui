import { isEmpty } from '../../shared/utils/string.util';
import { HttpParam, toStringParams } from '../http-client/axios';

const { REACT_APP_AUTH_URL, REACT_APP_REDIRECT_URI } = process.env;
const authorize = (): void => {
  if (!isLoggedIn()) {
    const authParams = new URLSearchParams(location.search);
    const authState = authParams.get('state');
    const authCode = authParams.get('code');
    if (state() !== authState || isEmpty(authCode)) {
      requestCode();
    } else {
      localStorage.removeItem('authState');
      requestToken(authCode!);
    }
  }
};

export const isLoggedIn = (): boolean => {
  return !isEmpty(getToken());
};

export const getToken = (): string | null => {
  return localStorage.getItem('token');
};

const requestCode = (): void => {
  const authState = generateState();
  localStorage.setItem('authState', authState);
  const params = new HttpParam();
  params
    .set('response_type', 'code')
    .set('client_id', 'client')
    .set('scope', 'openid')
    .set('state', authState)
    .set('redirect_uri', REACT_APP_REDIRECT_URI!);
  location.replace(`${REACT_APP_AUTH_URL!}/oauth2/authorize${toStringParams(params)}`);
};

const generateState = (): string => {
  return Math.random().toString(36).substring(2, 13) + Math.random().toString(36).substring(2, 13);
};

const requestToken = async (code: string): Promise<void> => {
  const params = new HttpParam();
  params
    .set('grant_type', 'authorization_code')
    .set('code', code)
    .set('redirect_uri', REACT_APP_REDIRECT_URI!);
  fetch(`${REACT_APP_AUTH_URL!}/oauth2/token${toStringParams(params)}`, {
    method: 'POST',
    headers: {
      authorization: `basic ${window.btoa('client:admin')}`
    }
  }).then(async (value) => {
    const token: any = await value.json();
    localStorage.setItem('token', token.access_token);
    localStorage.setItem('idToken', token.id_token);
    REACT_APP_REDIRECT_URI && location.replace(REACT_APP_REDIRECT_URI);
  });
};

const state = (): string | null => {
  return localStorage.getItem('authState');
};

export const navigateToUserAccount = (): void => {
  window.open(`${REACT_APP_AUTH_URL!}`);
};

export const logout = (): void => {
  localStorage.clear();
  location.replace(`${REACT_APP_AUTH_URL!}/logout`);
};
export default authorize;
