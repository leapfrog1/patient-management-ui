import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { MessageResponse } from '../../shared/constants/message-response';
import { HttpErrorResponse } from './http-response/message-response';

const baseURL = 'http://127.0.0.1:8080/api/v1';

interface Param {
  [key: string]: string;
}

export class HttpParam {
  private param: Param = {};
  set(key: string, value: string): HttpParam {
    this.param = { ...this.param, [key]: value };
    return this;
  }

  get(): Param {
    return this.param;
  }
}
function handleError(error: AxiosError<HttpErrorResponse>): void {
  const httpErrorResponse: HttpErrorResponse = getErrorMessage(error);
  console.error(httpErrorResponse);
}

function getErrorMessage(error: AxiosError<HttpErrorResponse>): HttpErrorResponse {
  const errorResponse = MessageResponse.httpErrorResponse;
  if (navigator.onLine) {
    return error?.response?.data ?? errorResponse.serverDown;
  } else {
    return errorResponse.offline;
  }
}

export async function get<T>(apiURL: string, httpParam?: HttpParam): Promise<T> {
  const config: AxiosRequestConfig = {
    params: httpParam != null ? httpParam.get() : null
  };
  return await axios
    .get(`${baseURL}${apiURL}`, config)
    .then((response: AxiosResponse) => {
      return response.data;
    })
    .catch((error: AxiosError<HttpErrorResponse>) => {
      handleError(error);
    });
}

export async function post<T, R>(
  apiURL: string,
  requestBody?: T,
  httpParam?: HttpParam
): Promise<R> {
  const config: AxiosRequestConfig = {
    params: httpParam != null ? httpParam.get() : null
  };
  return await axios
    .post(`${baseURL}${apiURL}`, requestBody, config)
    .then((response: AxiosResponse) => {
      return response.data;
    })
    .catch((error: AxiosError<HttpErrorResponse>) => {
      handleError(error);
    });
}

export async function patch<T, R>(
  apiURL: string,
  requestBody?: T,
  httpParam?: HttpParam
): Promise<R> {
  const config: AxiosRequestConfig = {
    params: httpParam != null ? httpParam.get() : null
  };
  return await axios
    .patch(`${baseURL}${apiURL}`, requestBody, config)
    .then((response: AxiosResponse) => {
      return response.data;
    })
    .catch((error: AxiosError<HttpErrorResponse>) => {
      handleError(error);
    });
}

export const toStringParams = (params: HttpParam): string => {
  return Object.entries(params.get()).reduce(
    (acc, [key, value], index, array) =>
      (acc += `${key}=${value}${index < array.length - 1 ? '&' : ''}`),
    '?'
  );
};
