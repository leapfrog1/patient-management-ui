interface BaseMessageResponse {
  code?: number;
  message?: string;
}

interface HttpError {
  field?: string;
  rejectedValue?: Object;
  errorMessages?: Set<BaseMessageResponse>;
}

export interface HttpSuccessResponse extends BaseMessageResponse {}

export interface HttpErrorResponse extends BaseMessageResponse {
  status?: string;
  timestamp?: string;
  description?: string;
  httpErrors?: Set<HttpError>;
}
