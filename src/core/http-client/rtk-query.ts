import { FetchBaseQueryArgs } from '@reduxjs/toolkit/dist/query/fetchBaseQuery';
import {
  BaseQueryFn,
  fetchBaseQuery,
  FetchArgs,
  FetchBaseQueryError,
  FetchBaseQueryMeta
} from '@reduxjs/toolkit/query/react';

const baseUrl = 'http://127.0.0.1:8080/api/v1';

const fetchQuery = (
  queryAttributes?: FetchBaseQueryArgs
): BaseQueryFn<string | FetchArgs, unknown, FetchBaseQueryError, {}, FetchBaseQueryMeta> => {
  const prepareHeaders = (): Headers => {
    return new Headers({
      authorization: `bearer ${localStorage.getItem('token') ?? ''}`
    });
  };
  return fetchBaseQuery({
    baseUrl,
    prepareHeaders,
    ...queryAttributes
  });
};

export default fetchQuery;
