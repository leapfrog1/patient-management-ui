import { configureStore } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import patientReducer from '../../pages/patient/redux/patient-slice';
import { patientService } from '../../pages/patient/service/patient-service';
import { errorInterceptor } from '../middleware/error-interceptor';

export const store = configureStore({
  reducer: {
    [patientService.reducerPath]: patientService.reducer,
    patient: patientReducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(patientService.middleware, errorInterceptor)
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch: () => AppDispatch = useDispatch;
