import React, { PureComponent } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';

import Patient from '../../pages/patient/patients';
import { paths } from '../../shared/constants/route.constant';
import Layout from '../layout/layout';
import PrivateRoute from './protected-routes';

export interface RouteDef {
  path: string;
  actionBar?: JSX.Element,
  element: JSX.Element,
  isProtected?: boolean
}

const routes: RouteDef[] = [
  {
    path: '*',
    element: <Navigate to={paths.home} replace={true} />,
    actionBar: <Patient.ActionBar className="w-100"></Patient.ActionBar>,
    isProtected: true
  },
  {
    path: paths.home,
    element: <Navigate to={paths.patients} replace={true} />,
    actionBar: <Patient.ActionBar className="w-100"></Patient.ActionBar>,
    isProtected: true
  },
  {
    path: paths.patients,
    actionBar: <Patient.ActionBar className="w-100"></Patient.ActionBar>,
    element: <Patient></Patient>,
    isProtected: true
  }
];

class Router extends PureComponent {
  render(): React.ReactNode {
    return (
      <BrowserRouter>
        <Layout>
          <Routes>
            {
              routes.map(value =>
                <Route key={value.path} {...value} element={
                  <PrivateRoute>
                    {value.element}
                  </PrivateRoute>
                }></Route>
              )
            }
          </Routes>
        </Layout>
      </BrowserRouter>
    );
  }
}

export default Router;
