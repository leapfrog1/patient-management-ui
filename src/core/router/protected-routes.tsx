import React, { FC } from 'react';
import authorize, { isLoggedIn } from '../oauth2/authenticator';

const PrivateRoute: FC<{ children: JSX.Element }> = ({ children }: { children: JSX.Element }) => {
  if (!isLoggedIn()) {
    authorize();
    return <React.Fragment></React.Fragment>;
  }
  return children;
};
export default PrivateRoute;
