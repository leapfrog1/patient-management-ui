import { HttpErrorResponse } from '../http-client/http-response/message-response';

let errorHandlers: HttpErrorHandler;

export interface ErrorHandler {
  handle: (errorMessage?: HttpErrorResponse) => void;
}
interface HttpErrorHandler {
  [key: number]: new () => ErrorHandler;
}

export function NetworkError(statusCode: number) {
  return (target: new () => ErrorHandler) => {
    errorHandlers = {
      ...errorHandlers,
      [statusCode]: target
    };
  };
}

export const getErrorHandler = (statusCode: number): ErrorHandler => {
  return errorHandlers[statusCode] && new errorHandlers[statusCode]();
};
