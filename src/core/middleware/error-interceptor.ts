import { isRejectedWithValue, Middleware, MiddlewareAPI } from '@reduxjs/toolkit';
import { HttpStatusCode } from '../../shared/constants/status-codes.contant';
import { logout } from '../oauth2/authenticator';
import { ErrorHandler, getErrorHandler, NetworkError } from './error-handler';

export const errorInterceptor: Middleware = (api: MiddlewareAPI) => (next) => (action) => {
  if (isRejectedWithValue(action)) {
    const { status } = action.payload;
    getErrorHandler(status).handle();
  }
  return next(action);
};

@NetworkError(HttpStatusCode.Unauthorized)
export class UnauthorizedHandler implements ErrorHandler {
  handle(): void {
    logout();
  }
}

@NetworkError(HttpStatusCode.Forbidden)
export class ForbiddenHandler implements ErrorHandler {
  handle(): void {
    logout();
  }
}
