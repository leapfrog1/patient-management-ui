import React from 'react';

const ActionBar: React.FC<{ children: React.ReactNode }> = (props) => {
    return (
        <div className="action-bar flex flex--ac bg-primary primary-border">
            {props.children}
        </div>
    );
};

export default ActionBar;
