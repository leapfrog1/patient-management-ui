import * as React from 'react';
import authorize, { isLoggedIn } from '../oauth2/authenticator';
import ActionBar from './action-bar/action-bar';
import { Header } from './header/header';
import Main from './main/main';
import NavBar from './navigation-bar/navigation-bar';

class Layout extends React.PureComponent<{ children: React.ReactNode }, { actionBar: React.ReactNode }> {
  onMenuChange(link: string): void {
    this.setActionBarElement(link);
  }

  setActionBarElement(link: string): void {
    this.setState({
      actionBar: [...(this.props.children as React.ReactElement)?.props?.children]
        .find(value => value.props.path === link)?.props?.actionBar
    });
  }

  componentDidMount(): void {
    this.onMenuChange(window.location.pathname);
  }

  renderActionBar(): React.ReactNode {
    if (!this.state?.actionBar) {
      return null;
    }
    return (
      <ActionBar>
        {this.state?.actionBar}
      </ActionBar>
    );
  }

  render(): React.ReactNode {
    return (
      <div>
        <Header></Header>
        <div className="content p-2r">
          <div className='flex'>
            <NavBar onMenuChange={(link) => this.onMenuChange(link)} />
            {
              this.renderActionBar()
            }
          </div>
          <Main>
            {this.props.children}
          </Main>
        </div>
      </div>
    );
  }
}

export default Layout;
