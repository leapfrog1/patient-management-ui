import { Avatar } from '../../../shared/components/avatar/avatar';
import { Logo } from '../../../shared/components/icons/logo/logo';

export const Header = (): JSX.Element => {
    return (
        <header className="header p-2r bg-primary flex flex--ac flex--jsb">
            <Logo></Logo>
            <Avatar></Avatar>
        </header>
    );
};
