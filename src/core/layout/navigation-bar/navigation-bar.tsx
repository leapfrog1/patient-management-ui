import React from 'react';
import { Menu } from '../../../shared/components/menu/menu';
import MenuItem from '../../../shared/components/menu/menu-item';
import Menus, { MenuProps } from '../../../shared/components/menu/menu-item.constant';
import { paths } from '../../../shared/constants/route.constant';

interface NavBarState {
  menus: MenuProps[]
}

interface NavBarProps {
  onMenuChange: (link: string) => void
}

class NavBar extends React.Component<NavBarProps, NavBarState> {
  state: Readonly<NavBarState> = {
    menus: Menus
  };

  onMenuChange(link: string): void {
    link = !this.state.menus.some(menu => menu.link === link) ? paths.patients : link;
    const { menus } = this.state;
    menus.forEach(data => (data.active = data.link === link));
    this.setState({ menus });
    this.props.onMenuChange(link);
  }

  componentDidMount(): void {
    this.onMenuChange(window.location.pathname);
  }

  render(): React.ReactNode {
    return (
      <nav className='navbar z-10 flex flex-alc position-rel mb-8r mw-20r'>
        <Menu>
          {
            this.state.menus.map(menu =>
              <MenuItem {...menu} key={menu.name} menuChange={(menu) => this.onMenuChange(menu.link)} />
            )
          }
        </Menu>
      </nav>
    );
  }
}

export default NavBar;
